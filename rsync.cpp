#include <sstream>
#include <thread>
#include <dirent.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>

#include "rsync.h"

void reader(int fd, char* ptr, size_t count) {
    int shift = 0;
    while (int read_bytes = read(fd, ptr + shift, count)) {
        count -= read_bytes;
        shift += read_bytes;
    }
}

void writer(int fd, char* ptr, size_t count) {
    int shift = 0;
    while (int write_bytes = write(fd, ptr + shift, count)) {
        count -= write_bytes;
        shift += write_bytes;
    }
}

void SocketConnection::WriteFrame(Frame* frame) {
    int len = frame->body.length();
    writer(fd_, (char*) &len, 4);
    int id = static_cast<int> (frame->msg_id);
    writer(fd_, (char*) &id, 1);
    writer(fd_, (char*) frame->body.c_str(), len);
}

void SocketConnection::ReadFrame(Frame* frame) {
    int len;
    reader(fd_, (char*) &len, 4);
    uint8_t msg_id;
    reader(fd_, (char*) &msg_id, 1);
    frame->msg_id = MsgId(msg_id);
    frame->body.resize(len);
    reader(fd_, (char*) frame->body.c_str(), len);
}

void Protocol::SendGetList() {
    Frame f;
    f.msg_id = MsgId::GETLIST;
    conn_->WriteFrame(&f);
}

void Protocol::SendFileList(const FileList& list) {
    Frame f;
    f.msg_id = MsgId::FILELIST;
    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << list;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

FileList Protocol::ReceiveFileList() {
    std::stringstream ss;
    FileList list;
    ss << last_received_.body;
    boost::archive::text_iarchive archive(ss);
    archive >> list;
    return list;
}

MsgId Protocol::RecvMsg() {
    conn_->ReadFrame(&last_received_);
    return last_received_.msg_id;
}

FileList GetFileList(std::string path) {
    FileList list;
    struct dirent *entry;
    std::vector<std::string> vec;
    const char* p_c_str = path.c_str();
    DIR *dir;
    dir = opendir(p_c_str);
    if (dir) {
        while ((entry = readdir(dir)) != NULL) {
            if (access (entry->d_name, 0)) {
                vec.push_back(entry->d_name);
            }
            
        }
    } else {
        throw std::runtime_error("Can not open directory");
    }
    closedir(dir);
    list.files = vec;
    return list;
}

std::vector<std::string> CompareLists(std::vector<std::string> source, std::vector<std::string> dest) {
    std::vector<std::string> v;
    for (size_t i = 0; i < dest.size(); ++i) {
        bool flag = false;
        for (size_t j = 0; j < source.size(); ++j) {
            if (dest[i] == source[j]) {
                flag = true;
            }
        }
        if (flag == false) {
            v.push_back(dest[i]);
        }
    }
    return v;
}

std::vector<std::string> ShareRes(std::vector<std::string> v1, std::vector<std::string> v2) {
    std::vector<std::string> output;
    for (size_t i = 0; i < v2.size(); ++i) {
        output.push_back("rm " + v2[i]);
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        output.push_back("cp " + v1[i]);
    }
    return output;
}

void Protocol::SendOk() {
    Frame f;
    f.msg_id = MsgId::OK;
    f.body = "";
    conn_->WriteFrame(&f);
}

int Sender::run() {
    SendGetList();
    bool flag = true;
    while (flag) {
        RecvMsg();
        if (last_received_.msg_id == MsgId::FILELIST) {
            FileList dest_files = ReceiveFileList();
            FileList source_files = GetFileList(source_);
            std::vector<std::string> diff = ShareRes(CompareLists(dest_files.files, source_files.files), CompareLists(source_files.files, dest_files.files));
            for (size_t i = 0; i < diff.size(); ++i) {
                std::cout << diff[i] << "\n";
            }
            flag = false;
        }
    }
    return 0;
}

int Receiver::run() {
    bool flag = true;
    while (flag) {
        RecvMsg();
        if (last_received_.msg_id == MsgId::GETLIST) {
            FileList files = GetFileList(dest_);
            SendFileList(files);
        flag = false;
        }
    }
    return 0;
}

// void rsync(std::string source, std::string dest) {
//     int socks[2];
//     std::cout << socks[0] << socks[1] << "\n";
//     socketpair(AF_UNIX, SOCK_STREAM, 0, socks);
//     std::cout << socks[0] << socks[1] << "\n";
//     SocketConnection s(socks[0]), r(socks[1]);
//     Sender sender(&s, source, dest);
//     Receiver receiver(&r);

//     std::thread t1([&] {
//         sender.run();
//     });

//     std::thread t2([&] {
//         receiver.run();
//     });
//     t1.join();
//     t2.join();
// }
