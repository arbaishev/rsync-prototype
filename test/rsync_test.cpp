#include "rsync.h"
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <gtest/gtest.h>

class FakeConnection : public Connection {
public:
    Frame frame;
    virtual void WriteFrame(Frame* f) { frame = *f; }
    virtual void ReadFrame(Frame* f) { *f = frame; }
};


class FakeReceiver : public Receiver {
public:
    FakeReceiver(SocketConnection* conn, std::string dest) : Receiver(conn, dest) {}
    auto get_conn() {
        return conn_;
    }
};

int CompareVectors(std::vector<std::string> &res, std::vector<std::string> &out) {
    size_t success_times = 0;
    for (int i = 0; i < res.size(); ++i) {
        for (int j = 0; j < out.size(); ++j) {
            if (res[i] == out[j]) {
                success_times++;
                break;
            }
        }
    }
    if (out.size() == success_times) {
        return 0;
    } else {
        return 1;
    }
}

TEST(Protocol, SendReceiveFileList) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));
    std::string source("/source");
    std::string dest("/home/albert/cs/ejudge/asm");
    SocketConnection soc1(socks[0]);
    SocketConnection soc2(socks[1]);
    Sender s(&soc1, source);
    FakeReceiver r(&soc2, dest);
    FileList true_files_list = GetFileList(dest);

    r.SendFileList(true_files_list);
    ASSERT_EQ(s.RecvMsg(), MsgId::FILELIST);
    FileList res = s.ReceiveFileList();
    ASSERT_EQ(0, CompareVectors(res.files, true_files_list.files));

}

TEST(Protocol, SendGetList) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));

    std::string dest("/home/home/home");
    std::string source("/source"); 
    SocketConnection soc1(socks[0]);
    SocketConnection soc2(socks[1]);
    Sender s(&soc1, source);
    FakeReceiver r(&soc2, dest);

    s.SendGetList();
    Frame f;
    r.get_conn()->ReadFrame(&f);
    ASSERT_EQ(f.msg_id, MsgId::GETLIST);
    ASSERT_EQ(0, strcmp(f.body.c_str(), dest.c_str()));

}

TEST(SenderReceiver, GetFileList) {
    std::string path("/home/albert/projects/test");
    FileList res = GetFileList(path);
    FileList my_res;
    my_res.files = {"test1", "test2", "test3", "test4", "test5", "test6"};
    ASSERT_EQ(res.files.size(), my_res.files.size());
    ASSERT_EQ(0, CompareVectors(res.files, my_res.files));
    ASSERT_EQ(6, res.files.size());
}

TEST(Compare, ShareRes) {
    std::vector<std::string> sor = {"c"};
    std::vector<std::string> des = {"d"};
    std::vector<std::string> res = ShareRes(sor, des);
    std::vector<std::string> my_res;
    std::string p;
    p = "rm d";
    my_res.push_back(p);
    p = "cp c";
    my_res.push_back(p);
    ASSERT_EQ(0, strcmp(res[0].c_str(), my_res[0].c_str()));
    ASSERT_EQ(0, strcmp(res[1].c_str(), my_res[1].c_str()));
}

TEST(SocketConnection, ReadWrite) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));

    SocketConnection sender(socks[0]), receiver(socks[1]);

    Frame frame1, frame2;
    frame1.msg_id = MsgId::OK;
    frame1.body = "body";
    sender.WriteFrame(&frame1);
    receiver.ReadFrame(&frame2);
    ASSERT_EQ(frame1.msg_id, frame2.msg_id);
    ASSERT_EQ(frame1.body, frame2.body);
}
