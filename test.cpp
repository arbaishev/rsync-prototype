#include <iostream>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <vector>

std::vector<std::string> GetFileListInDir(std::string path) {
    struct dirent *entry;
    std::vector<std::string> vec;
    const char* p_c_str = path.c_str();
    DIR *dir;
    dir = opendir(p_c_str);
    if (dir) {
        while ((entry = readdir(dir)) != NULL) {
            if (access (entry->d_name, 0)) {
                vec.push_back(entry->d_name);
            }
            
        }
    }
    closedir(dir);
    return vec;
}

std::vector<std::string> CompareLists(std::vector<std::string> source, std::vector<std::string> dest) {
    std::vector<std::string> v;
    for (size_t i = 0; i < dest.size(); ++i) {
        bool flag = false;
        for (size_t j = 0; j < source.size(); ++j) {
            if (dest[i] == source[j]) {
                flag = true;
            }
        }
        if (flag == false) {
            v.push_back(dest[i]);
        }
    }
    return v;
}

std::vector<std::string> ShareRes(std::vector<std::string> v1, std::vector<std::string> v2) {
    std::vector<std::string> output;
    for (size_t i = 0; i < v2.size(); ++i) {
        output.push_back("rm " + v2[i]);
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        output.push_back("cp " +v1[i]);
    }
    return output;
}

int main() {
    std::vector<std::string> vect1;
    std::vector<std::string> vect2;
    std::vector<std::string> rmcpv;
    std::string p1 = "/home/albert/projects/project-rsync (copy)/source";
    std::string p2 = "/home/albert/projects/project-rsync (copy)/dest";

    vect1 = GetFileListInDir(p1);
    vect2 = GetFileListInDir(p2);
    rmcpv = ShareRes(CompareLists(vect2, vect1), CompareLists(vect1, vect2));
    for (size_t i = 0; i < rmcpv.size(); ++i) {
        std::cout << rmcpv[i] << "\n";
    }

}