#include <sys/types.h>
#include <string>
#include <stdexcept>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <boost/variant.hpp>
#include <unistd.h>
#include <string>
#include <vector>
#include <cstdint>
#include <netinet/in.h>
#include <thread>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include "rsync.h"
namespace po = boost::program_options;

void start_sender(int server_rsnc, const std::string source) {
    SocketConnection s(server_rsnc);
    Sender sender(&s, source);

    std::thread t1([&] {
        sender.run();
    });

    t1.join();
}

void start_reciever(int client_rsnc, const std::string dest) {
    SocketConnection r(client_rsnc);
    Receiver receiver(&r, dest);

    std::thread t2([&] {
        receiver.run();
    });
    
    t2.join();
}

class Server{
private:
    int port = 6378, sockfd;
    std::string source;
public:
    Server(const std::string source_, int port_) {
        port = port_;
        source = source_;
        int opt = 1;
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

        if (sockfd < 0) {
            throw std::runtime_error("Socket error");
        }

        struct sockaddr_in server;
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = INADDR_ANY;
        server.sin_port = htons( port );
        
        if( bind(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0) {
            close(sockfd);
            throw std::runtime_error("Bind failed. Error");
        }
        if (listen(sockfd, 2) < 0) {
            fprintf(stderr, "listen failed: %s\n", strerror(errno));
            exit(1);
        }
    }

    void ServWait() {
        int client_sock, c;
        struct sockaddr_in client;
        c = sizeof(struct sockaddr_in);
        client_sock = accept(sockfd, (struct sockaddr *)&client, (socklen_t*) & c);
        if (client_sock < 0) {
            throw std::runtime_error("Connection failed. Serv Error");
        }
        try{
            start_sender(client_sock, source);
        }
        catch(std::runtime_error a){
            perror(a.what());
        }
        close(client_sock);
    }
    ~Server(){
        close(sockfd);
    }
};


int Client(const std::string dest_, std::string ip_, int port_) {
    std::string ip = ip_;
    int port = port_;
    std::string dest = dest_;
    struct sockaddr_in server;
    int sock;
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock < 0) printf("Could not create socket");

    server.sin_addr.s_addr = inet_addr(ip.c_str());
    server.sin_family = AF_INET;
    server.sin_port = htons( port );
 
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0) {
        throw std::runtime_error("Connect failed. Client Error");
    }
    try{
        start_reciever(sock, dest);
    }
    catch(std::runtime_error a){
        perror(a.what());
    }
    close(sock);
}

// std::string sender_args(const po::variables_map& vm) {
//     std::string port, src;
//     if(vm.count("port"))
//         port = vm["port"].as<std::string>();
//     if(vm.count("source"))
//         src = vm["source"].as<std::string>();
//     return port, src;
// }

// std::string reciever_args(const po::variables_map& vm) {
//     std::string ip, port, dst;
//     if(vm.count("dest"))
//         dst = vm["dest"].as<std::string>();
//     if (vm.count("ip"))
//         ip = vm["ip"].as<std::string>();
//     if(vm.count("port"))
//         port = vm["port"].as<std::string>();
//     std::cout<<"socket: "<<ip<<":"<<port<<std::endl;
//     return (ip, port);
// }

int main (int argc, char const *argv[]) {
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("sender", "start server with sender")
        ("reciever", "start client with reciever")
    ;

    po::options_description dir_desc("Input directories");
    dir_desc.add_options()
    ("source,s", po::value<std::string>(), "source directory")
    ("dest,d", po::value<std::string>(), "destination directory")
    ;

    po::options_description socket_config("socket config");
    socket_config.add_options()
    ("ip,i", po::value<std::string>(), "Input ip")
    ("port,p", po::value<std::string>(), "Input port")
    ;

    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(desc).allow_unregistered().run();
    po::store(parsed, vm);
    po::notify(vm);
    if (vm.count("help")) {
        desc.add(dir_desc);
        desc.add(socket_config);
        std::cout << desc << "\n";
        return 1;
    }

    if (vm.count("sender")) {
        desc.add(dir_desc);
        desc.add(socket_config);
        po::store(po::parse_command_line(argc,argv,desc), vm);
        std::string port, src;
        // port, src = sender_args(vm);
        if(vm.count("port"))
            port = vm["port"].as<std::string>();
        if(vm.count("source"))
            src = vm["source"].as<std::string>();
        Server Server(src, atoi(port.c_str()));
        Server.ServWait();
    }

    if (vm.count("reciever")) {
        desc.add(dir_desc);
        desc.add(socket_config);
        po::store(po::parse_command_line(argc,argv,desc), vm);
        std::string ip, port, dst;
        // (ip, port) = reciever_args(vm);
        if(vm.count("dest"))
            dst = vm["dest"].as<std::string>();
        if (vm.count("ip"))
            ip = vm["ip"].as<std::string>();
        if(vm.count("port"))
            port = vm["port"].as<std::string>();
        Client(dst, ip, atoi(port.c_str()));
    }    
}