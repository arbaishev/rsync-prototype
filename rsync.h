#pragma once

#include <vector>
#include <string>
#include <utility>

#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

enum class MsgId {
    GETLIST = 1,
    FILELIST = 2,
    REMOVE = 3,
    FILE = 4,
    FILE_PART = 5,
    OK = 6
};

struct Frame {
    MsgId msg_id;
    std::string body;
};

class Connection {
public:
    virtual ~Connection() {}
    virtual void WriteFrame(Frame* frame) = 0;
    virtual void ReadFrame(Frame* frame) = 0;
};

class SocketConnection {
public:
    SocketConnection(int fd) : fd_(fd) {}
//    ~SocketConnection();

    virtual void WriteFrame(Frame* frame);
    virtual void ReadFrame(Frame* frame);

private:
    int fd_;
};

struct FileList {
    std::vector<std::string> files;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & files;
    }
};

class Protocol {
public:
    Protocol(SocketConnection* conn) : conn_(conn) {}
    
    void SendGetList();
    void SendFileList(const FileList& list);
    void SendOk();
    // FileList ReceiveFileList();
    MsgId RecvMsg(); // reads next frame
    FileList ReceiveFileList();

    ~Protocol() {}
protected:
    SocketConnection* conn_;
    Frame last_received_;
};

class Sender : public Protocol {
public:
    Sender(SocketConnection* conn, std::string source) : Protocol(conn) {
        source_.resize(source.length());
        source_ = source;
        
    }
    int run();
private:
    std::string source_;
    std::string dest_;
};


class Receiver : public Protocol {
public:
    Receiver(SocketConnection* conn, std::string dest) : Protocol(conn) {
        // dest_ = "";
        dest_.resize(dest.length());
        dest_ = dest;
    }
    int run();

private:
    std::string dest_;    
};

FileList GetFileList(std::string path);

void rsync(std::string source, std::string dest);

std::vector<std::string> ShareRes(std::vector<std::string> v1, std::vector<std::string> v2);

std::vector<std::string> CompareLists(std::vector<std::string> source, std::vector<std::string> dest);

